const gulp = require('gulp-v4');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const rimraf = require('rimraf');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const spritesmith = require('gulp.spritesmith');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

// Static server
gulp.task('server', function() {
    browserSync.init({
        server: {
            port: 9000,
            baseDir: "build" //куда будет смотреть сервер
        }
    });

    gulp.watch('build/**/*').on('change', browserSync.reload);
});


// JS
gulp.task('js', function () {
    return gulp.src([
        //'src/js/validation.js',
        //'src/js/form.js',  //можно было бы написать src/js/**/*.js но так мы не контролируем последовательность подключения файлов, и если нам надо строгую последовательность, то нам лучше сделать, как сделано тут (либо использовать import
        //'src/js/navigation.js',
        'src/js/main.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'));
});

// PUG
gulp.task('templatesComp', function buildHTML() {
    return gulp.src('src/templates/*.pug')
        .pipe(pug({
            pretty: true //чтобы выходной html был не в одну строчку
        }))
        .pipe(gulp.dest('build'))
});

// SASS
gulp.task('sass', function () {
    var plugins = [
        autoprefixer(),
        cssnano()
    ];
    return gulp.src('src/styles/main.sass')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(rename('main.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'));
});

// SPRITES
gulp.task('sprite', function (cb) {
    var spriteData = gulp.src('src/img/icons/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../img/sprite.png',
        cssName: 'sprite.sass'
    }));
    spriteData.img.pipe(gulp.dest('build/img/'));
    spriteData.css.pipe(gulp.dest('src/styles/global/'));
    cb();
});


//CLEAR build
gulp.task('clean', function del(cb) {
    return rimraf('build', cb);
});

//COPY fonts
gulp.task('copyFonts', function () {
    return gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest('build/fonts'))
});

//COPY images
gulp.task('copyImages', function () {
    return gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('build/img'))
});

// Full COPY
gulp.task('copy', gulp.parallel('copyFonts', 'copyImages'));

//Watchers
gulp.task('watch', function () {
    gulp.watch('src/templates/**/*.pug', gulp.series('templatesComp'));
    gulp.watch('src/styles/**/*.sass', gulp.series('sass'));
    gulp.watch('src/js/**/*.js', gulp.series('js'));
});

//DEFAULT

gulp.task('default', gulp.series(
    'clean',
    gulp.parallel('templatesComp', 'sass', 'js', 'sprite', 'copy'),
    gulp.parallel('watch', 'server')
    )
);

//BUILD
gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('templatesComp', 'sass', 'js', 'sprite', 'copy')
    )
);