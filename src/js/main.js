window.addEventListener('load', function () {
    var images = document.getElementsByClassName('galery__picture');
    var show = document.getElementsByClassName('galery__big')[0];
    var img = document.createElement('img');
    var flagShow = false

    document.body.addEventListener('mousedown', closeShow);

    function closeShow(e) {
        show.classList.remove('galery__big--show');
        if(!e.target.parentNode.classList.contains('galery__picture')){
            flagShow = false;
        }
    }

    for(var key = 0; key < images.length; key++){
        images[key].addEventListener('click', function () {
            var src = this.getElementsByTagName('img')[0].src;
            if(!flagShow){
                img.setAttribute('src', src);
                show.appendChild(img);
                show.classList.add('galery__big--show');
            }
            flagShow = !flagShow;
        });
    }
});